package u03

import org.junit.*
import org.junit.Assert.*

class StreamTest:
  import u03.Streams.*
  import u03.Streams.Stream.*
  import Lists.List.*

  var str: Stream[Int] = Stream.iterate(0)(_ + 1)

  @Test def testTake() =
    str = Stream.filter(str)(x => x < 3 || x > 20)
    assertEquals(Cons(0, Cons(1,Cons(2,Cons(21,Cons(22,Cons(23, Nil())))))),
      Stream.toList(Stream.take(Stream.filter(str)(x => x < 3 || x > 20))(6)))

  @Test def testConstant() =
    assertEquals(Cons("x", Cons("x", Cons("x", Cons("x", Cons("x", Nil()))))), Stream.toList(Stream.take(constant("x"))(5)))
    assertEquals(Cons("", Cons("", Cons("", Nil()))), Stream.toList(Stream.take(constant(""))(3)))

  @Test def testDrop() =
    assertEquals(Stream.empty(), Stream.drop(Stream.take(Stream.iterate(0)( _ * 2))(2))(5))
    assertEquals(Cons(3, Cons(4, Cons(5, Cons(6, Cons(7, Nil()))))),
      Stream.toList(Stream.drop(Stream.take(Stream.iterate(1)( _ + 1))(7))(2)))

  @Test def testFibonacci() =
    assertEquals(Cons(0, Cons(1, Cons(1, Cons(2, Cons(3, Cons(5, Cons(8, Cons(13, Cons(21, Cons(34, Nil())))))))))),
      Stream.toList(Stream.take(Stream.fibs)(10)))
    assertEquals(Nil(), Stream.toList(Stream.take(Stream.fibs)(0)))
