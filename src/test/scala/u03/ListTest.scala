package u03

import org.junit.*
import org.junit.Assert.*

class ListTest:
  import u03.Lists.List.*
  import u03.Lists.*
  import u02.Modules.*;
  import u02.Modules.Person.*

  val l: List[Int] = Cons(10, Cons(20, Cons(30, Nil())))
  val listPeople: List[Person] = Cons(Student("elia", 2), Cons(Teacher("mario", "algebra"), Cons(Student("elisa", 3),
    Cons(Teacher("emma", "programmazione"), Nil()))))
  val people: List[Person] = Cons(Student("enrica", 2), Cons(Teacher("enzo", "strutture dati"), Cons(Student("anna", 3),
    Cons(Teacher("mauro", "programmazione di reti"), Cons(Teacher("asia", "strutture dati"), Nil())))))

  @Test def testSum() =
    assertEquals(0, sum(Nil()))
    assertEquals(60, sum(l))

  @Test def testMap() =
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))), map(l)(_ + 1))
    assertEquals(Cons("10", Cons("20", Cons("30", Nil()))), map(l)(_ + ""))

  @Test def testFilter() =
    assertEquals(Cons(20, Cons(30, Nil())), filter(l)(_ >= 20))
    assertEquals(Cons(10, Cons(30, Nil())), filter(l)(_ != 20))

  @Test def testAppend() =
    val tail = Cons(40, Nil())
    assertEquals(Cons(10, Cons(20, Cons(30, Cons(40, Nil())))), append(l, tail))

  @Test def testFlatMap() =
    assertEquals(Cons(11, Cons(21, Cons(31, Nil()))), flatMap(l)(v => Cons(v + 1, Nil())))
    assertEquals(Cons(11, Cons(12, Cons(21, Cons(22, Cons(31, Cons(32, Nil())))))), flatMap(l)(v => Cons(v + 1, Cons(v + 2, Nil()))))
    assertEquals(Cons(20, Cons(10, Cons(40, Cons(20, Cons(60, Cons(30, Nil())))))), flatMap(l)(v => Cons(v * 2, Cons(v, Nil()))))
    assertEquals(Cons(30,Cons(60,Cons(90,Nil()))), mapFlatMap(l)(_ * 3))
    assertEquals(Cons(20, Nil()), filterFlatMap(l)(_ % 20 == 0))

  @Test def testFold() =
    val lst = Cons(6, Cons(7, Cons(10, Cons(5, Nil()))))
    val bool = Cons(false, Cons(false, Cons(true, Cons(false, Nil()))))
    assertEquals(7, foldRight(lst)(3)( _ - _))
    assertEquals(8400, foldLeft(lst)(4)( _ * _ ))
    assertEquals(true, foldRight(bool)(true)( _ || _ ))
    assertEquals(false, foldLeft(bool)(true)( _ && _ ))

  import u02.Optionals.*
  import u02.Optionals.Option.*

  @Test def testMax() =
    assertEquals(Some(30), max(l))
    assertEquals(None(), max(Nil()))

  @Test def testListPerson() =
    assertEquals(Cons("algebra", Cons("programmazione", Nil())), listOfCourses(listPeople))
    assertEquals(Nil(), listOfCourses(Nil()))
    assertEquals(Cons("strutture dati", Cons("programmazione di reti", Cons("strutture dati", Nil()))),
      listOfCourses(people))
