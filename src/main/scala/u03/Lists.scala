package u03

import u02.Modules.Person.*
import u02.Modules.*

object Lists extends App:

  // A generic linkedlist
  enum List[E]:
    case Cons(head: E, tail: List[E])
    case Nil()
  // a companion object (i.e., module) for List
  object List:

    import u02.Optionals.*
    import u02.Optionals.Option.*
    import u02.Modules.Person

    def sum(l: List[Int]): Int = l match
      case Cons(h, t) => h + sum(t)
      case _ => 0

    def map[A, B](l: List[A])(mapper: A => B): List[B] = l match
      case Cons(h, t) => Cons(mapper(h), map(t)(mapper))
      case Nil() => Nil()

    def mapFlatMap[A, B](l: List[A])(mapper: A => B): List[B] = l match
      case Cons(h, t) => flatMap(l)(h => Cons(mapper(h), Nil()))
      case _ => Nil()

    def filter[A](l1: List[A])(pred: A => Boolean): List[A] = l1 match
      case Cons(h, t) if pred(h) => Cons(h, filter(t)(pred))
      case Cons(_, t) => filter(t)(pred)
      case Nil() => Nil()

    def filterFlatMap[A](l: List[A])(pred: A => Boolean): List[A] = l match
      case Cons(h, t) if pred(h) => append(flatMap(Cons(h, Nil()))(c => Cons(c, Nil())), filterFlatMap(t)(pred))
      case Cons(h, t) => filterFlatMap(t)(pred)
      case _ => Nil()

    def drop[A](l: List[A], n: Int): List[A] = l match
      case Cons(_, t) if n > 0 => drop(t, n-1)
      case Cons(h, t) => Cons(h, t)
      case _ => Nil()

    def append[A](left: List[A], right: List[A]): List[A] = (left, right) match
      case (Cons(h, t), _) => Cons(h, append(t, right))
      case (_, Cons(h, t)) => Cons(h, t)
      case _ => Nil()

    def flatMap[A,B](l: List[A])(f: A => List[B]): List[B] = l match
      case Cons(h1, t1) => append(f(h1), flatMap(t1)(f))
      case _ => Nil()

    def max(l: List[Int]): Option[Int] =
      @annotation.tailrec
      def _maximum(l: List[Int], acc: Option[Int]): Option[Int] = l match
        case Cons(h, t) if isEmpty(acc) || h > orElse(acc, 0) => _maximum(t, Some(h))
        case Cons(h, t) => _maximum(t, acc)
        case _ => acc
      _maximum(l, None())

    def listOfCourses(l: List[Person]): List[String] = l match
        case Cons(h, t) => h match
          case Teacher(_, c) => append(Cons(c, Nil()), listOfCourses(t))
          case _ => listOfCourses(t)
        case _ => Nil()

    @annotation.tailrec
    def foldLeft[A](l: List[A])(x: A)(f: (A, A) => A): A = l match
      case Cons(h, t) => foldLeft(t)(f(x, h))(f)
      case _ => x

    def foldRight[A](l: List[A])(x: A)(f: (A, A) => A): A = l match
      case Cons(h, t) => f(h, foldRight(t)(x)(f))
      case _ => x

  val l = List.Cons(10, List.Cons(20, List.Cons(30, List.Nil())))
  println(List.sum(l)) // 60

  import List.*

  println(sum(map(filter(l)(_ >= 20))(_ + 1))) // 21+31 = 52






